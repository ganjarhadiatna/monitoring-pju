<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['prefix' => 'lampu'], function () {
    // ui
    Route::get('', 'LampuController@index')->name('ui-lampu');
    Route::get('/search', 'LampuController@search')->name('ui-lampu-search');
    Route::get('/create', 'LampuController@create')->name('ui-lampu-create');
    Route::get('/edit/{id}', 'LampuController@edit')->name('ui-lampu-edit');
    Route::get('/monitoring/{id}', 'LampuController@monitoring')->name('ui-lampu-monitoring');

    // crud
    Route::post('/save', 'LampuController@save')->name('ui-lampu-save');
    Route::post('/update', 'LampuController@update')->name('ui-lampu-update');
    Route::post('/delete', 'LampuController@delete')->name('ui-lampu-delete');
});

Route::group(['prefix' => 'monitoring'], function () {
    // ui
    Route::get('', 'MonitoringController@index')->name('ui-monitoring');
    Route::get('/create', 'MonitoringController@create')->name('ui-monitoring-create');
    Route::get('/edit/{id}', 'MonitoringController@edit')->name('ui-monitoring-edit');

    // crud
    Route::post('/save', 'MonitoringController@save')->name('ui-monitoring-save');
    Route::post('/update', 'MonitoringController@update')->name('ui-monitoring-update');
    Route::post('/delete', 'MonitoringController@delete')->name('ui-monitoring-delete');
    Route::post('/delete-all', 'MonitoringController@deleteAll')->name('ui-monitoring-delete-all');
    Route::post('/delete-by-idlampu', 'MonitoringController@deleteByIdlampu')->name('ui-monitoring-delete-by-idlampu');
});
