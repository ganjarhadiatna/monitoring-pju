@extends('layouts.admin')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12 wrapper">
            <div class="card" style="width: 100%;">
                <div class="card-header wrapper">
                    <div class="mr-auto">
                        <h3 style="margin-top: 6px;">Dashboard</h3>
                    </div>
                    <div class="ml-auto"></div>
                </div>
                @foreach($lampu as $dt)
                    <div class="card-body wrapper">
                        <div style="width: 100%;">
                            <div class="card">
                                <div class="card-body wrapper">
                                    <div class="mr-auto">
                                        <i class="fa fa-4x fa-lightbulb" style="color: #17a2b8;"></i>
                                    </div>
                                    <div class="ml-auto" style="text-align: right;">
                                        <h3>{{ $dt->nama }}</h3>
                                        <p>Lampu</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div style="width: 100%; margin: 0 15px;">
                            <div class="card">
                                <div class="card-body wrapper">
                                    <div class="mr-auto">
                                        <i class="fa fa-4x fa-circle" style="color: {{ $dt->status == 0 ? 'grey' : 'green' }};"></i>
                                    </div>
                                    <div class="ml-auto" style="text-align: right;">
                                        <h3>{{ $dt->status == 0 ? 'Lampu Mati' : 'Lampu Hidup' }}</h3>
                                        <p>Status</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div style="width: 100%;">
                            <div class="card">
                                <div class="card-body wrapper">
                                    <div class="mr-auto">
                                        <i class="fa fa-4x fa-chart-line" style="color: red;"></i>
                                    </div>
                                    <div class="ml-auto" style="text-align: right;">
                                        <h3>{{ $dt->daya ? $dt->daya : '0' }}</h3>
                                        <p>Daya</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
@endsection
