@extends('layouts.admin')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">

            <div class="card">
                <div class="card-header wrapper">
                    <div class="mr-auto">
                        <h3 style="margin-top: 6px;">Data Monitoring @isset($profile) {{$profile->nama}} @endif</h3>
                    </div>
                    <div class="ml-auto">
                        <button class="btn btn-danger" data-toggle="modal" data-target="#deleteModal">
                            <i class="fa fa-lw fa-trash-alt"></i>
                        </button>

                        <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                <div class="modal-header"> 
                                    <h5 class="modal-title" id="deleteModalLabel">Peringatan</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    Data akan dihapus secara permanen, lanjutkan?
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
                                    <a class="btn btn-primary" 
                                        href="{{ isset($id) ? route('ui-monitoring-delete-by-idlampu') : route('ui-monitoring-delete-all') }}"
                                        onclick="event.preventDefault(); document.getElementById('id-form').submit();"
                                        >
                                        Lanjutkan
                                    </a>

                                    <form id="id-form" action="{{ isset($id) ? route('ui-monitoring-delete-by-idlampu') : route('ui-monitoring-delete-all') }}" method="POST" style="display: block;">
                                        @csrf
                                        <input type="hidden" name="id" value="{{ isset($id) ? $id : '0' }}" />
                                    </form>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card-body">
                    @isset($chMonitoring)
                        <div style="width: 100%; margin-bottom: 15px;">
                            {!! $chMonitoring->container() !!}
                        </div>
                    @endif
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th scope="col">NO</th>
                                <th scope="col">Lampu</th>
                                <th scope="col">Lokasi</th>
                                <th scope="col">Tegangan</th>
                                <th scope="col">Arus</th>
                                <th scope="col">Daya</th>
                                <th scope="col">Frekuensi</th>
                                <th scope="col">Status Baterai</th>
                                <th scope="col">Tanggal</th>
                                <!-- <th width="170"></th> -->
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i = 1; ?>
                            @foreach($data as $dt)
                                <?php $map = "https://www.openstreetmap.org/#map=19/".$dt->longitude."/".$dt->latitude ?>
                                <tr>
                                    <th scope="row">{{ $i }}</th>
                                    <td>{{ $dt->nama }}</td>
                                    <td>
                                        <a href="{{ $map }}" target="_blank">
                                            {{ $dt->lokasi }} <i class="fa fa-1x fa-map-marker-alt" ></i>
                                        </a>
                                    </td>
                                    <td>{{ $dt->tegangan }}</td>
                                    <td>{{ $dt->arus }}</td>
                                    <td>{{ $dt->daya }}</td>
                                    <td>{{ $dt->frekuensi }}</td>
                                    <td>{{ $dt->daya_faktor }}</td>
                                    <td>{{ $dt->created_at }}</td>
                                    <!-- <td>
                                        <button class="btn btn-primary">
                                            <i class="fa fa-lw fa-eye"></i>
                                        </button>
                                        <button class="btn btn-warning">
                                            <i class="fa fa-lw fa-pencil-alt"></i>
                                        </button>
                                        <button class="btn btn-danger">
                                            <i class="fa fa-lw fa-trash-alt"></i>
                                        </button>
                                    </td> -->
                                </tr>
                                <?php $i++; ?>
                            @endforeach
                        </tbody>
                    </table>
                    {{ $data->links() }}
                </div>
            </div>
            
        </div>
    </div>
</div>
@isset($chMonitoring)
    {!! $chMonitoring->script() !!}
@endif
@endsection
