<nav id="sidebar">
    <div class="sidebar-container">
        <div class="sidebar-header">
            <h3>{{ 'PJU DISTARKIM' }}</h3>
        </div>

        <ul class="list-unstyled components">
            <li>
                <a href="{{ route('home') }}">Dashboard</a>
            </li>
            <li>
                <a href="{{ route('ui-lampu') }}">Data Lampu</a>
            </li>
            <li>
                <a href="{{ route('ui-monitoring') }}">Data Monitoring</a>
            </li>
        </ul>
    </div>
</nav>