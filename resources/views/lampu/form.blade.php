@extends('layouts.admin')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">

            <div class="card">
                <div class="card-header wrapper">
                    <div class="mr-auto">
                        @if(isset($data))
                            <h3 style="margin-top: 6px;">Ubah Lampu</h3>
                        @else
                            <h3 style="margin-top: 6px;">Tambah Lampu</h3>
                        @endif
                    </div>
                    <div class="ml-auto"></div>
                </div>

                <div class="card-body">
                    <form method="POST" action="@if(isset($data)) {{ route('ui-lampu-update') }} @else {{ route('ui-lampu-save') }} @endif">
                        @csrf
                        
                        @if(isset($data))
                            <input type="hidden" name="id" value="{{ $data->id }}">
                        @endif
                        
                        <div class="form-group">
                            <label for="inputKode">KODE</label>
                            <input type="text" value="@if(isset($data)) {{ $data->kode }} @endif" class="form-control @error('kode') is-invalid @enderror" id="inputKode" name="kode" required>
                            @error('kode')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        
                        <div class="form-group">
                            <label for="inputNama">Nama lampu</label>
                            <input type="text" value="@if(isset($data)) {{ $data->nama }} @endif" class="form-control @error('nama') is-invalid @enderror" id="inputNama" name="nama" required>
                            @error('nama')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="inputLokasi">Lokasi</label>
                            <input type="text" value="@if(isset($data)) {{ $data->lokasi }} @endif" class="form-control @error('lokasi') is-invalid @enderror" id="inputLokasi" name="lokasi" required>
                            @error('lokasi')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="inputLongitude">Longitude</label>
                            <input type="text" value="@if(isset($data)) {{ $data->longitude }} @endif" class="form-control @error('longitude') is-invalid @enderror" id="inputLongitude" name="longitude" required>
                            @error('longitude')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="inputLatitude">Latitude</label>
                            <input type="text" value="@if(isset($data)) {{ $data->latitude }} @endif" class="form-control @error('latitude') is-invalid @enderror" id="inputLatitude" name="latitude" required>
                            @error('latitude')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <div class="form-group">
                                <label for="inputStatus">Status Lampu</label>
                                <select value="@if(isset($data)) {{ $data->status }} @endif" class="form-control @error('status') is-invalid @enderror" id="inputStatus" name="status">
                                @if(isset($data))
                                    @if($data->status == 0)
                                        <option value="0" selected>Lampu Mati</option>
                                        <option value="1">Lampu Hidup</option>
                                    @endif
                                    @if($data->status == 1)
                                        <option value="0">Lampu Mati</option>
                                        <option value="1" selected>Lampu Hidup</option>
                                    @endif
                                @else
                                    <option value="0">Lampu Mati</option>
                                    <option value="1">Lampu Hidup</option>
                                @endif
                                </select>
                            </div>
                            @error('status')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </form>
                </div>
            </div>
            
        </div>
    </div>
</div>
@endsection
