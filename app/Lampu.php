<?php   
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Lampu extends Model{
    protected $table = "lampu";

    // protected $fillable = [];

    // public $timestamps = false;

    public function scopeGetAll($query, $limit)
    {
        return $this
        ->select(
            'lampu.id',
            'lampu.kode',
            'lampu.nama',
            'lampu.status',
            'lampu.lokasi',
            'lampu.longitude',
            'lampu.latitude',
            'lampu.created_at',
            'lampu.updated_at',
            (DB::raw('(select tegangan from monitoring where lampu_id=lampu.id order by id desc limit 1) as tegangan ')),
            (DB::raw('(select arus from monitoring where lampu_id=lampu.id order by id desc limit 1) as arus ')),
            (DB::raw('(select daya from monitoring where lampu_id=lampu.id order by id desc limit 1) as daya ')),
            (DB::raw('(select frekuensi from monitoring where lampu_id=lampu.id order by id desc limit 1) as frekuensi ')),
            (DB::raw('(select daya_faktor from monitoring where lampu_id=lampu.id order by id desc limit 1) as daya_faktor '))
        )
        ->orderBy('lampu.id', 'desc')
        ->paginate($limit);
    }

    public function scopeGetAllSearch($query, $limit, $serch)
    {
        return $this
        ->select(
            'lampu.id',
            'lampu.kode',
            'lampu.nama',
            'lampu.status',
            'lampu.lokasi',
            'lampu.longitude',
            'lampu.latitude',
            'lampu.created_at',
            'lampu.updated_at',
            (DB::raw('(select tegangan from monitoring where lampu_id=lampu.id order by id desc limit 1) as tegangan ')),
            (DB::raw('(select arus from monitoring where lampu_id=lampu.id order by id desc limit 1) as arus ')),
            (DB::raw('(select daya from monitoring where lampu_id=lampu.id order by id desc limit 1) as daya ')),
            (DB::raw('(select frekuensi from monitoring where lampu_id=lampu.id order by id desc limit 1) as frekuensi ')),
            (DB::raw('(select daya_faktor from monitoring where lampu_id=lampu.id order by id desc limit 1) as daya_faktor '))
        )
        ->where('kode', 'like', '%' . $serch . '%')
        ->orWhere('nama', 'like', '%' . $serch . '%')
        ->orderBy('id', 'desc')
        ->orderBy('lampu.id', 'desc')
        ->paginate($limit);
    }
}