<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Monitoring;
use App\Lampu;

class MonitoringController extends Controller
{
    public function index()
    {
        $data = Monitoring::GetAll(10);
        return view('monitoring.index', ['data' => $data]);
    }

    public function status(Request $request)
    {
        $id = $request->input('id');
        $data = Lampu::where(['id' => $id])->first();

        if ($data->status == '0')
        {
            Lampu::where(['id' => $id])->update(['status' => '1']);

            $out = [
                'message' => 'Lampu Hidup',
                'status' => 'ok',
                'status_lampu' => '1',
                'code' => 201
            ];
        }
        else 
        {
            Lampu::where(['id' => $id])->update(['status' => '0']);

            $out = [
                'message' => 'Lampu Mati',
                'status' => 'ok',
                'status_lampu' => '0',
                'code' => 201
            ];
        }

        return response()->json($out, $out['code']);
    }

    public function save(Request $request)
    {
        $status = $request->input('status');
        $kode = $request->input('kode');
        $tegangan = $request->input('tegangan');

        $data = Lampu::where(['kode' => $kode])->first();

        if ($data->status == '0') 
        {
            $payload = [
                'status' => '0',
                "updated_at" => date('Y-m-d H:i:s')
            ];

            Lampu::where(['id' => $data->id])->update($payload);

            $response = [
                'message' => 'Lampu Mati',
                'status' => 'ok',
                'status_lampu' => '0',
                'code' => 201
            ];
        }
        else
        {
            $payload = [
                'status' => '1',
                "updated_at" => date('Y-m-d H:i:s')
            ];

            Lampu::where(['id' => $data->id])->update($payload);

            $payload2 = [
                'tegangan' => $tegangan == '0' ? '0' : $request->input('tegangan'),
                'arus' => $tegangan == '0' ? '0' : $request->input('arus'),
                'daya' => $tegangan == '0' ? '0' : $request->input('daya'),
                'frekuensi' => $tegangan == '0' ? '0' : $request->input('frekuensi'),
                'daya_faktor' => $tegangan == '0' ? '0' : $request->input('daya_faktor'),
                'lampu_id' => $data->id,
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ];

            Monitoring::insert($payload2);

            $response = [
                'message' => 'Lampu Mati',
                'status' => 'ok',
                'status_lampu' => '1',
                'code' => 201
            ];
        }

        return $response['status_lampu'];
    }

    public function get()
    {
        $data = Lampu::getAll();
        if ($data) 
        {
            $out = [
                'message' => 'success',
                'status' => 'ok',
                'data' => $data,
                'code' => 201
            ];
        }
        else 
        {
            $out = [
                'message' => 'error',
                'status' => 'ok',
                'code' => 401
            ];
        }
        return response()->json($out, $out['code']);
    }

    public function delete(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
        ]);

        $id = $request->input('id');

        $service = Monitoring::where(['id' => $id])->delete();

        if ($service) 
        {
            return redirect('/monitoring');
        }
        else 
        {
            return redirect('/monitoring');
        }
    }

    public function deleteAll()
    {
        $service = Monitoring::truncate();

        if ($service) 
        {
            return redirect('/monitoring');
        }
        else 
        {
            return redirect('/monitoring');
        }
    }

    public function deleteByIdlampu(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
        ]);

        $id = $request->input('id');

        $service = Monitoring::where(['lampu_id' => $id])->delete();

        if ($service) 
        {
            return redirect('/lampu/monitoring/' . $id);
        }
        else 
        {
            return redirect('/lampu/monitoring/' . $id);
        }
    }
}