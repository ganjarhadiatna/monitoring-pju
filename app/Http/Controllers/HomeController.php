<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Lampu;
use App\Monitoring;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $lampu = Lampu::GetAll(10);
        $monitoring = Monitoring::GetAll(10);
        return view('home', ['lampu' => $lampu, 'monitoring' => $monitoring]);
    }
}
