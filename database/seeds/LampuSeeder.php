<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LampuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            'id' => '1',
            'kode' => 'L001',
            'nama' => 'Lampu Jalan',
            'lokasi' => 'Jl. mana weh',
            'status' => '0',
            'users_id' => '1',
            "created_at" => '2020-06-28 19:08:45',
            "updated_at" => '2020-06-28 19:08:45'
        ];

        DB::table('lampu')->insert($data);
    }
}
