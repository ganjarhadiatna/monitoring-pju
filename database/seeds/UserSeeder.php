<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            'id' => '1',
            'name' => 'Admin',
            'email' => 'admin@gmail.com',
            'password' => '$2y$10$SAxExoRE0MLJojGiG44uMuh1zGd2uClaoW7qNab1PzYVhsrGh.i0S',
            "created_at" => '2020-06-28 19:08:45',
            "updated_at" => '2020-06-28 19:08:45'
        ];

        DB::table('users')->insert($data);
    }
}
