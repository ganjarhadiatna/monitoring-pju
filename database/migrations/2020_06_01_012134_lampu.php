<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Lampu extends Migration{
    
    public function up(){
        Schema::create('lampu', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('kode', 4)->unique();
            $table->string('nama');
            $table->string('lokasi')->nullable();
            $table->string('longitude')->nullable();
            $table->string('latitude')->nullable();
            $table->string('lokasi')->nullable();
            $table->enum('status', ['0', '1', '2', '3'])->default('0');
            $table->unsignedBigInteger('users_id');
            $table->timestamps();

            $table->foreign('users_id')->references('id')->on('users');
        });
    }

    public function down(){
        Schema::dropIfExists('lampu');
    }
}