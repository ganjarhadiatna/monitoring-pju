<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Monitoring extends Migration{
    
    public function up(){
        Schema::create('monitoring', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('tegangan')->nullable();
            $table->string('arus')->nullable();
            $table->string('daya')->nullable();
            $table->string('frekuensi')->nullable();
            $table->string('daya_faktor')->nullable();
            $table->unsignedBigInteger('lampu_id');
            $table->timestamps();

            $table->foreign('lampu_id')->references('id')->on('lampu');
        });
    }

    public function down(){
        Schema::dropIfExists('monitoring');
    }
}